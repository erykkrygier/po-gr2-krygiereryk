package pl.edu.uwm.wmii.krygiereryk.laboratorium11;

import java.time.LocalDate;

public class Main {

    public static void main(String[] args) {
        Integer[] intarr = new Integer[]{1,5,8,4,3};
        ArrayUtil.selectionSort(intarr);
        for(Integer i:intarr){
            System.out.println(i);
        }
        LocalDate[] datearr=new LocalDate[]{LocalDate.parse("2020-01-01"),
                LocalDate.parse("2020-01-13"),LocalDate.parse("1999-12-04")};
        ArrayUtil.selectionSort(datearr);
        for (LocalDate j:datearr){
            System.out.println(j);
        }
    }
}
