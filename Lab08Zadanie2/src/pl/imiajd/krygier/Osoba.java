package pl.imiajd.krygier;

import java.time.LocalDate;
import java.util.Arrays;

public abstract class Osoba
{
    public Osoba(String nazwisko, String[] imiona, LocalDate dataUrodzenia, boolean plec)
    {
        this.nazwisko = nazwisko;
        this.imiona=imiona;
        this.dataUrodzenia=dataUrodzenia;
        this.plec=plec;
    }

    public abstract String getOpis();

    public String getNazwisko()
    {
        return nazwisko;
    }

    private String nazwisko;

    private String[] imiona;

    public String getImiona(){
        return Arrays.toString(imiona);
    }

    private LocalDate dataUrodzenia;
    public LocalDate getDataUrodzenia(){
        return dataUrodzenia;
    }

    private boolean plec;
    public String getPlec(){
        String mez="Mężczyzna";
        String kob="Kobieta";
        if (plec==true){
            return mez;
        }
        else{
            return kob;
        }
    }

}