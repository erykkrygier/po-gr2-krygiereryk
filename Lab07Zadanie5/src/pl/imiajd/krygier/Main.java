package pl.imiajd.krygier;

public class Main {

    public static void main(String[] args) {
        Osoba Janusz = new Osoba("Kowalski", 1975);
        Student Maciek = new Student("Nowak", 1998, "Informatyka");
        Nauczyciel Grażyna = new Nauczyciel("Kowalska", 1960, 2900);
        System.out.println(Janusz);
        System.out.println(Maciek);
        System.out.println(Grażyna);
    }
}