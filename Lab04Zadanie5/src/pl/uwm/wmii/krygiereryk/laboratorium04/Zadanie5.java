package pl.uwm.wmii.krygiereryk.laboratorium04;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Zadanie5 {
    static BigDecimal kapital (int n, BigDecimal k, BigDecimal p){
        BigDecimal zarobek=new BigDecimal("0");
        BigDecimal p100 =p.divide(BigDecimal.valueOf(100));
        BigDecimal plus1=p100.add(BigDecimal.valueOf(1));
        BigDecimal npotega = plus1.pow(n);
        zarobek=k.multiply(npotega);
        zarobek=zarobek.setScale(2, RoundingMode.CEILING);
        return zarobek;
    }
    public static void main(String[] args) {
        BigDecimal kapitał= new BigDecimal("5000");
        BigDecimal oprocentowanie =new BigDecimal("5");
	System.out.println(kapital(3,kapitał,oprocentowanie));
    }
}
