package pl.uwm.wmii.krygiereryk.laboratorium06;

public class RachunekBankowy {
    static double rocznaStopaProcentowa;
    private double saldo;

    RachunekBankowy(double s){
        saldo=s;
    }

    double ObliczMiesieczneOdsetki(){
        double odsetki=(saldo*rocznaStopaProcentowa)/12;
        saldo+=odsetki;
        System.out.println(saldo);
        return saldo;
    }
    static void setRocznaStopaProcentowa(double x){
        rocznaStopaProcentowa=x;
    }
}
