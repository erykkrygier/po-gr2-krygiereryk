package pl.imiajd.krygier;

public class Main {

    public static void main(String[] args) {
	Adres home = new Adres("Warszawska",1,"Olsztyn",12345);
    Adres home2 = new Adres("Olsztynska",3,4,"Warszawa",12345);
	home.pokaz();
	home2.pokaz();
	if(przed(home,home2)==true){
	    System.out.println("Dany adres wystepuje przed");
    }
	else{
	    System.out.println("Dany adres nie wystepuje przed");
    }

    }

    static class Adres{
        private String ulica;
        private int numer_domu;
        private int numer_mieszkania;
        private String miasto;
        private int kod_pocztowy;
        Adres(String ulica, int numer_domu, int numer_mieszkania,String miasto, int kod_pocztowy){
            this.ulica=ulica;
            this.numer_mieszkania=numer_mieszkania;
            this.numer_domu= numer_domu;
            this.miasto=miasto;
            this.kod_pocztowy=kod_pocztowy;

        }

        Adres(String ulica, int numer_domu,String miasto, int kod_pocztowy){
            this.ulica=ulica;
            this.numer_domu= numer_domu;
            this.miasto=miasto;
            this.kod_pocztowy=kod_pocztowy;
        }

        void pokaz() {
            if (numer_mieszkania != 0) {
                System.out.println("Kod pocztowy:" + kod_pocztowy + " " + "Miasto: " + miasto);
                System.out.println("Ul. " + ulica + " " + numer_domu + " Mieszkanie nr " + numer_mieszkania);
            }
            else{
                System.out.println("Kod pocztowy:" + kod_pocztowy + " " + "Miasto: " + miasto);
                System.out.println("Ul. " + ulica + " " + numer_domu);
                }
             }

        }
    static public boolean przed(Adres a, Adres b){
        if(a.kod_pocztowy==b.kod_pocztowy){
            return true;
        }
        else{
            return false;
        }
    }
}
