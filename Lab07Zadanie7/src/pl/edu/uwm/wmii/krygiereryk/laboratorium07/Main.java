package pl.edu.uwm.wmii.krygiereryk.laboratorium07;

import java.awt.*;

public class Main {

    public static void main(String[] args) {
        BetterRectangle test = new BetterRectangle(6,5,10,20);
        System.out.println("Pole prostokąta wynosi: "+ test.GetArea());
        System.out.println("Obwód prostokąta wynosi: "+ test.getPerimeter());
    }
    static class BetterRectangle extends Rectangle {
        BetterRectangle (int x,int y,int width, int height){
            super();
            setLocation(x,y);
            setSize(width,height);
        }
        public int getPerimeter (){
            return (2*width+2*height);
        }
        public int GetArea(){
            return (width*height);
        }
    }
}