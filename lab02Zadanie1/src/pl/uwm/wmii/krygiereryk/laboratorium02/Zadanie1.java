package pl.uwm.wmii.krygiereryk.laboratorium02;

import java.util.Scanner;
import java.lang.Math;
public class Zadanie1 {

    public static void main(String[] args) {
        Scanner podaj = new Scanner(System.in);
        System.out.println("Podaj liczbe od 1 do 100 ");
        int n = podaj.nextInt();
        int[] tablica = new int[n];
        int[] tablica2 = new int[n];
        int nieparzyste = 0;
        int ujemne = 0;
        int dodatnie = 0;
        int zera = 0;
        int sumaujemnych = 0;
        int sumadodatnich = 0;
        int dlugoscdodatnich = 0;
        int pamiec = 0;
        if (n < 1 || n > 100) {
            System.out.println("Podałeś/aś zła liczbe! Spróbuj ponownie");
            main(null);
        }
        for (int i = 0; i < n; i++) {
            int max = 999;
            int min = -999;
            int zasieg = max - min + 1;
            tablica[i] = (int) (Math.random() * zasieg) + min;
        }
        for (int j = 0; j < n; j++) {
            System.out.println(tablica[j]);
        }
        for (int k : tablica) {
            if (k % 2 != 0) {
                nieparzyste += 1;
            }
            if (k > 0) {
                dodatnie += 1;
                sumadodatnich += k;
                pamiec += 1;
                if (pamiec > dlugoscdodatnich) {
                    dlugoscdodatnich = pamiec;
                }
            }
            if (k == 0) {
                zera += 1;
                pamiec += 1;
                if (pamiec > dlugoscdodatnich) {
                    dlugoscdodatnich = pamiec;
                }
            }
            if (k < 0) {
                ujemne += 1;
                sumaujemnych += k;
                pamiec = 0;
            }

        }
        for (int l = 0; l < n; l++) {
            if (tablica[l] > 0) {
                tablica2[l] = 1;
            }
            if (tablica[l] < 0) {
                tablica2[l] = -1;
            }
        }
        System.out.println("Tablica po zamianie wyglada nastepujaco:");
        for (int k : tablica2) {
            System.out.println(k);
        }
        System.out.println("Podaj lewy przedzial");
        int lewy = podaj.nextInt();
        System.out.println("Podaj prawy przedzial");
        int prawy = podaj.nextInt();
        for (int m = 0; m < (prawy - lewy) / 2 + lewy; m++) {
            int temp = tablica[lewy + m-1];
            tablica[lewy + m-1] = tablica[prawy - m -1];
            tablica[prawy - m-1] = temp;
        }
        System.out.println("Odwrocona tablica wyglada nastepujaco:");
        for (int x : tablica) {
            System.out.println(x);
        }
        int najwieksza=tablica[0];
        for (int c=0;c<n;c++){
            if (tablica[c]>=najwieksza){
                najwieksza=tablica[c];
            }
        }
        int ilerazy=0;
        for (int d=0;d<n;d++){
            if (tablica[d]==najwieksza){
                ilerazy+=1;
            }

        }
        System.out.println("Liczba nieparzystych:"+ nieparzyste );
        System.out.println("Liczba dodatnie:"+ dodatnie );
        System.out.println("Najwieksza liczba:"+ najwieksza+" występuje "+ilerazy+" razy" );
        System.out.println("Liczba ujemnych:"+ ujemne );
        System.out.println("Liczba zer:"+ zera );
        System.out.println("Suma ujemnych:"+ sumaujemnych );
        System.out.println("Suma dodatnich:"+ sumadodatnich );
        System.out.println("Najdłuższy ciąg liczb dodatnich:"+ dlugoscdodatnich );


    }
}

