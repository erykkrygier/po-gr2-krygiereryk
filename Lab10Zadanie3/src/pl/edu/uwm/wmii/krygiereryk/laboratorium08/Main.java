package pl.edu.uwm.wmii.krygiereryk.laboratorium08;

import java.nio.file.Path;
import java.util.ArrayList;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;

public class Main {

    public static void main(String[] args) throws IOException {
	ArrayList<String> odczytane = new ArrayList<String>();
	String [] temp = czytaj("plik.txt");
	for (String i:temp){
	    odczytane.add(i);
    }
	for(String i: odczytane){
	    System.out.println(i);
    }
        Collections.sort(odczytane);
    System.out.println(" ");
	System.out.println("Posortowane");
    System.out.println(" ");
	for(String j:odczytane){
	    System.out.println(j);
    }
    }
    static String[] czytaj(String sciezka) throws IOException {
        Path nazwapliku = Path.of(sciezka);
        String zdanie = Files.readString(nazwapliku);
        String[] str=zdanie.split("[\n]");
        return str;
    }
}
