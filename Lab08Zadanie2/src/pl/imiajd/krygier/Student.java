package pl.imiajd.krygier;

import java.time.LocalDate;

public class Student extends Osoba
{
    public Student(String nazwisko, String[] imiona, LocalDate dataUrodzenia, boolean plec, String kierunek, double sredniaOcen)
    {
        super(nazwisko,imiona,dataUrodzenia,plec);
        this.kierunek = kierunek;
        this.sredniaOcen=sredniaOcen;
    }

    public String getOpis()
    {
        return String.format("Student na kierunku: "+ getKierunek()+ " Średnia ocen: "+ getSredniaOcen());
    }

    private String kierunek;
    public String getKierunek(){
        return kierunek;
    }

    private double sredniaOcen;
    public double getSredniaOcen() {
        return sredniaOcen;
    }
}