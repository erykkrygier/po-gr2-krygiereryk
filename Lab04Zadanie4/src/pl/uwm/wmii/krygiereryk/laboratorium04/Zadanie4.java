package pl.uwm.wmii.krygiereryk.laboratorium04;

import java.math.BigInteger;

public class Zadanie4 {
    static BigInteger szachownica(int n){
        BigInteger a=new BigInteger("1");
        BigInteger b=new BigInteger("2");
        BigInteger suma=new BigInteger("0");
        int rozmiar =n*n;
        for (int i=0;i<rozmiar;i++){
            suma=suma.add(a);
            a=a.multiply(b);
        }
        return suma;
    }
    public static void main(String[] args) {
        System.out.println(szachownica(2));
    }
}

