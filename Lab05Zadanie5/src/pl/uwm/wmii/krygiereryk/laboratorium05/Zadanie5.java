package pl.uwm.wmii.krygiereryk.laboratorium05;

import java.util.ArrayList;

public class Zadanie5 {
    public static void reverse(ArrayList<Integer> a){
        ArrayList <Integer> temp=new ArrayList<>();
        for (int i=0;i<a.size();i++){
            temp.add(a.get(a.size()-1-i));
        }
        System.out.println(temp);
    }
    public static void main(String[] args) {
        ArrayList<Integer> x = new ArrayList<>();
        x.add(1);
        x.add(4);
        x.add(9);
        x.add(16);
        x.add(5);
        reverse(x);
    }
}
