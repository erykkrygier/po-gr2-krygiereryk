package pl.edu.uwm.wmii.krygiereryk.laboratorium12;

import java.util.LinkedList;

public class Main {

    public static void main(String[] args) {
        LinkedList<String> lista = new LinkedList<String>();
        lista.add("Kowalski");
        lista.add("Nowak");
        lista.add("Krygier");
        lista.add("Malinowski");
        lista.add("PanX");
        lista.add("PaniY");
        System.out.println(lista);
        odwroc(lista);
        System.out.println(lista);


        LinkedList<Integer> lista2 = new LinkedList<Integer>();
        lista2.add(1);
        lista2.add(2);
        lista2.add(3);
        lista2.add(4);
        lista2.add(5);
        lista2.add(6);
        System.out.println(lista2);
        odwroc(lista2);
        System.out.println(lista2);
    }
    public static <T>void odwroc (LinkedList<T> pracownicy){
        LinkedList<T> temp=new LinkedList<T>();
        for (int i=pracownicy.size()-1;i>=0;i--){
            temp.add(pracownicy.getLast());
            pracownicy.removeLast();
        }
        for (int j=temp.size()-1;j>=0;j--){
            pracownicy.add(temp.getFirst());
            temp.removeFirst();
        }
    }
}