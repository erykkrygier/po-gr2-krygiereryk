package pl.edu.uwm.wmii.krygiereryk.laboratorium13;

import java.util.HashMap;
import java.util.Scanner;
import java.util.TreeMap;

public class Main {
    public static void sortedArray(HashMap<String,String> x){
        TreeMap<String,String> sort = new TreeMap<>();
        sort.putAll(x);
        sort.entrySet().forEach(entry->{
            System.out.println(entry.getKey() + " Ocena: " + entry.getValue());
        });
    }
    public static void main(String[] args) {
        HashMap<String,String> studenci = new HashMap<>();
        studenci.put("Krygier","db");
        studenci.put("StudentX","bdb");
        studenci.put("StudentY","dst");
        while (true){
            Scanner scan = new Scanner(System.in);
            String x= scan.nextLine();
            if (x.equals("dodaj")){
                System.out.println("Kogo chcesz dodac");
                String y= scan.nextLine();
                String [] splitted = y.split("\\s+");
                studenci.put(splitted[0],splitted[1]);
            }
            if(x.equals("usun")){
                System.out.println("Kogo chcesz usunac?");
                String z= scan.nextLine();
                studenci.remove(z);
            }
            if (x.equals("aktualizuj")){
                String e= scan.nextLine();
                String [] splitted = e.split("\\s+");
                studenci.replace(splitted[0],splitted[1]);
            }
            if(x.equals("wypisz")){
                sortedArray(studenci);
            }
            if (x.equals("zakoncz")){
                break;
            }
        }
    }
}
