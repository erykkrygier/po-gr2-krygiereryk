package pl.edu.uwm.wmii.krygiereryk.laboratorium11;
import java.time.LocalDate;
public class Main {

    public static void main(String[] args) {
        Integer[] intarr = new Integer[]{1,5,8,4,3};
        LocalDate[] datearr = new LocalDate[]{LocalDate.parse("2020-01-01"),
                LocalDate.parse("2020-01-02"),LocalDate.parse("2020-01-03")};
        int liczba = 5;
        LocalDate data = LocalDate.parse("2020-01-03");

        System.out.println(ArrayUtil.binSearch(intarr,5));
        System.out.println(ArrayUtil.binSearch(intarr,9));
        System.out.println(ArrayUtil.binSearch(datearr,LocalDate.parse("2020-01-03")));
        System.out.println(ArrayUtil.binSearch(datearr,LocalDate.parse("2020-08-07")));
    }
}
