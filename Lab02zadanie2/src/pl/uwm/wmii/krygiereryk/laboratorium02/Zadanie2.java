package pl.uwm.wmii.krygiereryk.laboratorium02;

import java.util.Scanner;
import java.lang.Math;
public class Zadanie2 {

    public static void main(String[] args) {
        Scanner podaj = new Scanner(System.in);
        System.out.println("Podaj liczbe od 1 do 100 ");
        int n = podaj.nextInt();
        int[] tablica = new int[n];
        if (n < 1 || n > 100) {
            System.out.println("Podałeś/aś zła liczbe! Spróbuj ponownie");
            main(null);
        }
        for (int i = 0; i < n; i++) {
            int max = 999;
            int min = -999;
            int zasieg = max - min + 1;
            tablica[i] = (int) (Math.random() * zasieg) + min;
        }
        for (int j = 0; j < n; j++) {
            System.out.println(tablica[j]);
        }
        System.out.println("Ilość liczb nieparzystych wynosi:"+ileNieparzystych(tablica));
        System.out.println("Ilość liczb parzystych wynosi:"+ileParzystych(tablica));
        System.out.println("Najwieksza liczba wystepuje: "+ileMaksymalnych(tablica)+" razy");
        System.out.println("Ilość liczb odatnich wynosi:"+ileDodatnich(tablica));
        System.out.println("Ilość liczb ujemnych wynosi:"+ileUjemnych(tablica));
        System.out.println("Ilość zer wynosi:"+ileZerowych(tablica));
        System.out.println("Najdłuższy ciąg liczb dodatnich wynosi:"+dlugoscMaksymalnegoCiaguDodatnich(tablica));
        System.out.println("Suma liczb dodatnich wynosi:"+sumaDodatnich(tablica));
        System.out.println("Suma liczb ujemnych wynosi:"+sumaUjenych(tablica));
        signum(tablica);
        System.out.println("Podaj lewy przedzial");
        int lewy = podaj.nextInt();
        System.out.println("Podaj prawy przedzial");
        int prawy = podaj.nextInt();
        odwrocFragment(tablica,lewy,prawy);

    }
    public static int ileNieparzystych(int[]tab){
        int nieparzyste = 0;
        for (int i : tab) {
            if (i % 2 !=0) {
                nieparzyste += 1;
            }
        }
        return nieparzyste;
    }
    public static int ileParzystych(int[]tab){
        int parzyste = 0;
        for (int i : tab) {
            if (i % 2 ==0) {
                parzyste += 1;
            }
        }
        return parzyste;
    }
    public static int ileDodatnich(int[]tab){
        int dodatnie=0;
        for (int i: tab){
            if(i>0){
                dodatnie+=1;
            }
        }
        return dodatnie;
    }
    public static int ileUjemnych(int[]tab){
        int ujemne=0;
        for (int i: tab){
            if(i<0){
                ujemne+=1;
            }
        }
        return ujemne;
    }
    public static int ileZerowych(int[]tab){
        int zera=0;
        for (int i: tab){
            if(i<0){
                zera+=1;
            }
        }
        return zera;
    }
    public static int ileMaksymalnych(int[]tab){
        int najwieksza=tab[0];
        int ilerazy=0;
        for (int k : tab) {
            if (k > najwieksza) {
                najwieksza = k;
            }
        }
        for (int j:tab){
            if(j==najwieksza){
                ilerazy+=1;
            }
        }
        return ilerazy;
    }
    public static int sumaDodatnich(int[]tab){
        int sumadodatnich =0;
        for (int i : tab){
            if (i>0){
                sumadodatnich+=i;
            }
        }
        return sumadodatnich;
    }
    public static int sumaUjenych(int[]tab){
        int sumaujemnych =0;
        for (int i : tab){
            if (i>0){
                sumaujemnych+=i;
            }
        }
        return sumaujemnych;
    }

    public static int dlugoscMaksymalnegoCiaguDodatnich(int[]tab){
        int pamiec=0;
        int najwiekszy=0;
        for (int i: tab){
            if(i>0){
                pamiec+=1;
                if(pamiec>najwiekszy){
                    najwiekszy=pamiec;
                }
            }
            else {
                pamiec=0;
            }
        }
        return najwiekszy;
    }

    public static void signum(int[]tab){
        int [] tab2=new int[tab.length];
        for(int i =0;i<tab.length;i++){
            if(tab[i]>0){
                tab2[i]=1;
            }
            if(tab[i]<0){
                tab2[i]=-1;
            }
        }
        for (int j: tab2){
            System.out.println(j);
        }
    }
    public static void odwrocFragment(int[]tab,int lewy,int prawy){
        for (int i=0;i<(prawy-lewy)/2+lewy;i++){
            int temp= tab[lewy -i-1];
            tab[lewy +i-1] = tab[prawy-i-1];
            tab[prawy-i-1]=temp;
        }
        for (int j : tab){
            System.out.println(j);
            }
    }
}
