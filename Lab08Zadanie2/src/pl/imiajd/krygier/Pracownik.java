package pl.imiajd.krygier;

import java.time.LocalDate;

public class Pracownik extends Osoba
{
    public Pracownik(String nazwisko, String[] imiona, LocalDate dataUrodzenia, boolean plec, double pobory, LocalDate dataZatrudnienia)
    {
        super(nazwisko,imiona,dataUrodzenia,plec);
        this.pobory = pobory;
        this.dataZatrudnienia=dataZatrudnienia;
    }

    public double getPobory()
    {
        return pobory;
    }

    public String getOpis()
    {
        return String.format("Pensja: "+ getPobory()+ " Zatrudniony dnia: "+ getDataZatrudnienia());
    }

    private double pobory;

    private LocalDate dataZatrudnienia;
    public LocalDate getDataZatrudnienia(){
        return dataZatrudnienia;
    }
}
