package pl.imiajd.krygier;

import java.time.LocalDate;


public abstract class Instrument {
    private String producent;
    private LocalDate rokProdukcji;
    public Instrument(String producent, LocalDate rokProdukcji){
        this.producent=producent;
        this.rokProdukcji=rokProdukcji;
    }
    public String getProducent(){
        return producent;
    }
    public LocalDate getRokProdukcji(){
        return rokProdukcji;
    }
    @Override
    public String toString(){
        return  "Instrment: "+ getClass().getSimpleName()+" "
                +"Producent: "+ producent + " " +"Rok Produkcji: "+ rokProdukcji;
    }
    public boolean equals(Instrument i){
        if(i.getProducent()==producent){
            return true;
        }
        else{
            return false;
        }
    }
    public abstract String dzwięk();
}
