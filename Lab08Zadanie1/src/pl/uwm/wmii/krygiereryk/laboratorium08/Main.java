package pl.uwm.wmii.krygiereryk.laboratorium08;

import java.time.LocalDate;
import java.util.Arrays;

public class Main
{
    public static void main(String[] args)
    {
        LocalDate urodziny1 = LocalDate.of(1965,12,5);
        LocalDate urodziny2 = LocalDate.of(1999,5,27);
        LocalDate zatrudnienie = LocalDate.of(2009,4,13);
        String[] Imiona1 = new String[]{"Jan","Paweł"};
        String[] Imiona2 = new String[]{"Anna","Maria"};
        Osoba[] ludzie = new Osoba[2];
        ludzie[0] = new Pracownik("Kowalski",Imiona1,urodziny1,true,3000.34,zatrudnienie);
        ludzie[1] = new Student("Nowak", Imiona2,urodziny2,false,"Informatyka",4.25);
        // ludzie[2] = new Osoba("Dyl Sowizdrzał");

        for (Osoba p : ludzie) {
            System.out.println("Nazwisko: "+ p.getNazwisko() + " Imiona: "+p.getImiona()+   " Data urodzenia: "+p.getDataUrodzenia()+" Płeć:" + p.getPlec()+" " + p.getOpis());
        }
    }
}

abstract class Osoba
{
    public Osoba(String nazwisko,String[] imiona,LocalDate dataUrodzenia, boolean plec)
    {
        this.nazwisko = nazwisko;
        this.imiona=imiona;
        this.dataUrodzenia=dataUrodzenia;
        this.plec=plec;
    }

    public abstract String getOpis();

    public String getNazwisko()
    {
        return nazwisko;
    }

    private String nazwisko;

    private String[] imiona;

    public String getImiona(){
        return Arrays.toString(imiona);
    }

    private LocalDate dataUrodzenia;
    public LocalDate getDataUrodzenia(){
        return dataUrodzenia;
    }

    private boolean plec;
    public String getPlec(){
        String mez="Mężczyzna";
        String kob="Kobieta";
        if (plec==true){
            return mez;
        }
        else{
            return kob;
        }
    }

}

class Pracownik extends Osoba
{
    public Pracownik(String nazwisko,String[] imiona,LocalDate dataUrodzenia,boolean plec, double pobory,LocalDate dataZatrudnienia)
    {
        super(nazwisko,imiona,dataUrodzenia,plec);
        this.pobory = pobory;
        this.dataZatrudnienia=dataZatrudnienia;
    }

    public double getPobory()
    {
        return pobory;
    }

    public String getOpis()
    {
        return String.format("Pensja: "+ getPobory()+ " Zatrudniony dnia: "+ getDataZatrudnienia());
    }

    private double pobory;

    private LocalDate dataZatrudnienia;
    public LocalDate getDataZatrudnienia(){
        return dataZatrudnienia;
    }
}


class Student extends Osoba
{
    public Student(String nazwisko,String[] imiona,LocalDate dataUrodzenia,boolean plec, String kierunek,double sredniaOcen)
    {
        super(nazwisko,imiona,dataUrodzenia,plec);
        this.kierunek = kierunek;
        this.sredniaOcen=sredniaOcen;
    }

    public String getOpis()
    {
        return String.format("Student na kierunku: "+ getKierunek()+ " Średnia ocen: "+ getSredniaOcen());
    }

    private String kierunek;
    public String getKierunek(){
        return kierunek;
    }

    private double sredniaOcen;
    public double getSredniaOcen() {
        return sredniaOcen;
    }
}