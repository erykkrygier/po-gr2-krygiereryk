package pl.uwm.wmii.krygiereryk.laboratorium00;

public class Zadanie11 {

    public static void main(String[] args) {
	System.out.println("""
            Tak wygląda mój wielki maturalny sen:          
            siedzą w oknie dwie małpy przykute łańcuchem,                        
            za oknem fruwa niebo                        
            i kąpie się morze.                        
            \s                        
            Zdaję z historii ludzi.                        
            Jąkam się i brnę.                        
            \s                        
            Małpa wpatrzona we mnie, ironicznie słucha,                        
            druga niby to drzemie -\s                       
            a kiedy po pytaniu nastaje milczenie,                       
            podpowiada mi                      
            cichym brząkaniem łańcucha.""");
    }
}
