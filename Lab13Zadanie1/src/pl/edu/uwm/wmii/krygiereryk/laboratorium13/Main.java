package pl.edu.uwm.wmii.krygiereryk.laboratorium13;

import java.util.Arrays;
import java.util.PriorityQueue;
import java.util.Scanner;

class Zadania implements Comparable<Zadania>{
    int priorytet;
    String [] zadanie;
    public Zadania(String [] zadanie,int priorytet){
        this.priorytet=priorytet;
        this.zadanie=zadanie;
    }

    public int getPriorytet(){
        return priorytet;
    }
    @Override
    public String toString(){
        System.out.println("Zadanie: ");
        for(String i : zadanie){
         System.out.print(i+" ");
        }
        return " ";
    }

    @Override
    public int compareTo(Zadania o) {
        if (this.getPriorytet()>o.getPriorytet()){
            return 1;
        }
        else if (this.getPriorytet()<o.getPriorytet()){
            return -1;
        }
        else{
            return 0;
        }
    }
}
public class Main {

    public static void main(String[] args) {
        PriorityQueue<Zadania> ListaZadan= new PriorityQueue<>();
        while (true){
            Scanner scan = new Scanner(System.in);
            String x = scan.nextLine();
            String [] polecenia =x.split("\\s+");
            if (polecenia[0].equals("dodaj")){
                ListaZadan.add(new Zadania(Arrays.copyOfRange(polecenia,2,polecenia.length),Integer.parseInt(polecenia[1])));
            }
            if(polecenia[0].equals("nastepne")){
                ListaZadan.remove();
            }
            if (polecenia[0].equals("zakoncz")){
                break;
            }
        }
        while (!ListaZadan.isEmpty()) {
            System.out.println(ListaZadan.remove());
        }

    }
}
