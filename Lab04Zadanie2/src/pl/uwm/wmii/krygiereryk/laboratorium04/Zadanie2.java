package pl.uwm.wmii.krygiereryk.laboratorium04;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
public class Zadanie2 {
    static int policz(String sciezka,char znak)throws IOException{
    int suma=0;
    Path nazwapliku = Path.of(sciezka);
    String zdanie=Files.readString(nazwapliku);
    char [] str1=zdanie.toCharArray();
    for (char value: str1){
        if (znak==value){
            suma++;
            }
        }
        return suma;
    }
    public static void main(String[] args) throws IOException {
    System.out.println("Liczba wystapien znaku w pliku wynosi:"+policz("plik.txt", 'l'));
    }
}
