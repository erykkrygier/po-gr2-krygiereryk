package pl.imiajd.krygier;

public class Main {

    public static void main(String[] args) {
        Osoba Janusz = new Osoba("Kowalski", 1975);
        Student Maciek = new Student("Nowak", 1998, "Informatyka");
        Nauczyciel Grażyna = new Nauczyciel("Kowalska", 1960, 2900);
        System.out.println(Janusz);
        System.out.println(Maciek);
        System.out.println(Grażyna);
    }

    static class Osoba {
        private String nazwisko;
        private int rok_urodzenia;

        public Osoba(String nazwisko, int rok_urodzenia) {
            this.nazwisko = nazwisko;
            this.rok_urodzenia = rok_urodzenia;
        }

        public String getNazwisko() {
            return nazwisko;
        }

        public int getRok_urodzenia() {
            return rok_urodzenia;
        }

        @Override
        public String toString() {
            return String.format("Nazwisko: " + getNazwisko() + " Rok Urodzenia: " + getRok_urodzenia());
        }

    }

    private static class Student extends Osoba {
        private String kierunek;

        public Student(String nazwisko, int rok_urodzenia, String kierunek) {
            super(nazwisko, rok_urodzenia);
            this.kierunek = kierunek;
        }

        public String getKierunek() {
            return kierunek;
        }

        @Override
        public String toString() {
            return String.format("Nazwisko: " + getNazwisko() + " Rok Urodzenia: " + getRok_urodzenia() + " Kierunek: " + kierunek);
        }
    }

    static class Nauczyciel extends Osoba {
        private double pensja;

        public Nauczyciel(String nazwisko, int rok_urodzenia, double pensja) {
            super(nazwisko, rok_urodzenia);
            this.pensja = pensja;
        }

        public double getPensja() {
            return pensja;
        }

        @Override
        public String toString() {
            return String.format("Nazwisko: " + getNazwisko() + " Rok Urodzenia: " + getRok_urodzenia() + " Pensja: " + pensja);
        }
    }
}
