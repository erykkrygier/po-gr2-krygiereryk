package pl.edu.uwm.wmii.krygiereryk.laboratorium13;

import java.util.HashMap;
import java.util.Scanner;
import java.util.TreeMap;

class Student implements Comparable<Student>{
    String imie;
    String nazwisko;
    int id=0;
    public Student(String imie,String nazwisko,int id){
        this.id = id;
        this.imie = imie;
        this.nazwisko=nazwisko;
    }

    public int getId(){

        return this.id;
    }

    @Override
    public String toString() {

        return "Nazwisko: "+ this.nazwisko + " Imie: "+ this.imie + " ID: "+ this.id;
    }

    @Override
    public int compareTo(Student o) {
        int result = this.nazwisko.compareTo(o.nazwisko);
        if(result==0){
            result=this.imie.compareTo(o.imie);
        }
        if(result==0){
         result = Integer.compare(this.id,o.id);
        }
        return result;
    }
}

public class Main {
    public static void sortedArray(HashMap<Student,String> x){
        TreeMap<Student,String> sort = new TreeMap<>();
        sort.putAll(x);
        sort.entrySet().forEach(entry->{
            System.out.println(entry.getKey() + " Ocena: " + entry.getValue());
        });
    }
    public static void main(String[] args) {
        HashMap<Student,String> studenci = new HashMap<>();
        Student Krygier = new Student("Eryk","Krygier",2133);
        Student panKowalski = new Student("Jan","Kowalski",2077);
        Student panKowalski2= new Student("Andrzej","Kowalski",2121);
        Student panKowalski3= new Student("Andrzej","Kowalski",1111);
        Student paniNowak = new Student("Anna","Nowak",4444);
        studenci.put(Krygier,"db");
        studenci.put(panKowalski,"bdb");
        studenci.put(paniNowak,"dst");
        studenci.put(panKowalski2,"ndst");
        studenci.put(panKowalski3,"db");
        while (true){
            Scanner scan = new Scanner(System.in);
            String x= scan.nextLine();
            HashMap<Student,String> pomoc = new HashMap<>();
            if (x.equals("dodaj")){
                System.out.println("Kogo chcesz dodac");
                String y= scan.nextLine();
                String [] splitted = y.split("\\s+");
                studenci.put(new Student(splitted[0],splitted[1],Integer.parseInt(splitted[2])),splitted[3]);
                pomoc.put(new Student(splitted[0],splitted[1],Integer.parseInt(splitted[2])),splitted[3]);
            }
            if(x.equals("usun")){
                System.out.println("Kogo chcesz usunac?");
                String z= scan.nextLine();
                int identyfikator = Integer.parseInt(z);
                Student s = null;
                for(Student k : studenci.keySet()){
                    if(k.getId()==identyfikator){
                        s=k;
                        break;
                    }
                }
                if(s!=null){
                    studenci.remove(s);
                }
            }
            if (x.equals("aktualizuj")){
            String e= scan.nextLine();
            String [] splitted = e.split("\\s+");
            int identyfikator = Integer.parseInt(splitted[0]);
            Student s = null;
            for(Student k : studenci.keySet()){
                if(k.getId()== identyfikator){
                    s=k;
                    break;
                }

            }
            studenci.replace(s,splitted[1]);
            }
            if(x.equals("wypisz")){
                sortedArray(studenci);
            }
            if (x.equals("zakoncz")){
                break;
            }
        }
    }
}