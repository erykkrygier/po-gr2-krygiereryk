package pl.uwm.wmii.krygiereryk.laboratorium05;

import java.util.ArrayList;

public class Zadanie2 {
    public static ArrayList<Integer> merge (ArrayList<Integer>a, ArrayList<Integer>b){
        ArrayList<Integer> krotsza= new ArrayList<Integer>();
        ArrayList<Integer> dlozsza= new ArrayList<Integer>();
        ArrayList <Integer> nowa =new ArrayList<Integer>();

        if(a.size()>b.size()){
            krotsza=b;
            dlozsza=a;
        }
        else{
            krotsza=a;
            dlozsza=b;
        }
        for(int i=0;i<dlozsza.size();i++){
            if(i<krotsza.size()){
                nowa.add(a.get(i));
                nowa.add(b.get(i));
            }
            else{
                nowa.add(dlozsza.get(i));
            }
        }
        return nowa;
    }
    public static void main(String[] args) {
        ArrayList<Integer> x =new ArrayList<Integer>();
        x.add(1);
        x.add(4);
        x.add(9);
        x.add(16);
        ArrayList<Integer> y =new ArrayList<Integer>();
        y.add(9);
        y.add(7);
        y.add(4);
        y.add(9);
        y.add(11);
        y.add(45);
        System.out.println(merge(x,y));
    }
}
