package pl.edu.uwm.wmii.krygiereryk.laboratorium11;

import java.time.LocalDate;

public class Main {

    public static void main(String[] args) {
    Integer [] intniesort = new Integer[]{1,3,6,2,7};
    Integer [] intsort = new Integer[]{1,2,4,5,7,8};
    System.out.println(ArrayUtil.isSorted(intniesort));
    System.out.println(ArrayUtil.isSorted(intsort));
    LocalDate [] datesort = new LocalDate[]{LocalDate.parse("2020-08-17"),LocalDate.parse("2020-10-10"),LocalDate.parse("2021-01-01")};
    LocalDate [] dateniesort = new LocalDate[]{LocalDate.parse("2020-05-17"),LocalDate.parse("2020-02-10"),LocalDate.parse("2019-01-01")};
    System.out.println(ArrayUtil.isSorted(datesort));
    System.out.println(ArrayUtil.isSorted(dateniesort));
    }
}
