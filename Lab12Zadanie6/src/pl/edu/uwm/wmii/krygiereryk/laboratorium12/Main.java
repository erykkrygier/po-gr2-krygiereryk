package pl.edu.uwm.wmii.krygiereryk.laboratorium12;

import java.util.Scanner;
import java.util.Stack;

public class Main {

    public static void main(String[] args) {
	    Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbe: ");
        int liczba = scan.nextInt();
        Stack<Integer> stack=new Stack<Integer>();
        int a = liczba % 10;
        int b= ((liczba % 100)-a)/10;
        int c = ((liczba %1000)- b)/100;
        int d = (liczba-(liczba%1000))/1000;
        int x = String.valueOf(1000).length();
        stack.push(a);
        stack.push(b);
        stack.push(c);
        stack.push(d);
        while (!stack.isEmpty()){
            int temp = stack.peek();
            System.out.print(temp + " ");
            stack.pop();
        }
    }
}
