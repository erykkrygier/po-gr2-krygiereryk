package pl.edu.uwm.wmii.krygiereryk.laboratorium12;

import java.util.LinkedList;

public class Main {

    public static void main(String[] args) {
        LinkedList<String> lista = new LinkedList<String>();
        lista.add("Kowalski");
        lista.add("Nowak");
        lista.add("Krygier");
        lista.add("Malinowski");
        lista.add("PanX");
        lista.add("PaniY");
        System.out.println(lista);
        redukuj(lista,1);
        System.out.println(lista);

    }
    public static void redukuj (LinkedList<String> pracownicy,int n){
        for(int i=0;i<pracownicy.size();i++) {
            if (i % n == 0) {
                pracownicy.remove(i);
            }
        }
    }
}

