package pl.uwm.wmii.krygiereryk.laboratorium04;

public class Zadanie1 {
    static int countChar(String str, char c){
        int suma=0;
        char[] str1=str.toCharArray();
        for (char value : str1) {
            if (c == value) {
                suma++;
            }
        }
        return suma;
    }
    static int countSubStr(String str,String subStr){
        int suma=0;
        String[] str1=str.split(" ");
        for(int i =0;i<str1.length;i++){
            if (subStr.equals(str1[i])) {
                suma++;
            }
        }
        return suma;
    }
    static String middle(String str){
        String[] str1=str.split("");
        String mid = "";
        if (str1.length % 2==0){
            mid=str1[str1.length/2-1]+str1[str1.length/2];
        }
        if (str1.length % 2==1){
            mid=str1[str1.length/2];
        }
        return mid;
    }
    static String repeat(String str,int n){
        String temp=str;
        for (int i=1;i<n;i++){
            str+=temp;
        }
        return str;
    }
    static int [] where(String str, String subStr){
        String [] temp=str.split(" ");
        int [] tab= new int[temp.length];
        for (int i=0; i< temp.length;i++){
            if (subStr.equals(temp[i])){
                tab[i]=1;
            }
            else{
                tab[i]=0;
            }
        }
        for (int j: tab){
            System.out.print(j+" ");
        }
        return tab;
    }
     static String change(String str){
        char x=0;
        StringBuffer slowo = new StringBuffer();
        for (int i=0; i< str.length();i++){
            x = str.charAt(i);
            if(Character.isUpperCase(x)){
                x=Character.toLowerCase(x);
            }
            else {
                x=Character.toUpperCase(x);
            }
            slowo.append(x);
        }
        return slowo.toString();
    }
    static String nice(String str){
        char x =0;
        StringBuffer slowo =new StringBuffer();
        for (int i=0; i<str.length();i++){
            x=str.charAt(i);
            if ((str.length()-1-i)%3==0&&(str.length()-1-i)!=0){
                slowo.append(x+"'");
            }
            else{
                slowo.append(x);
            }
        }
    return slowo.toString();
    }
    static String nice1 (String str,String sep, int n){
        char x =0;
        StringBuffer slowo =new StringBuffer();
        for (int i=0; i<str.length();i++){
            x=str.charAt(i);
            if ((str.length()-1-i)%n==0&&(str.length()-1-i)!=0){
                slowo.append(x+sep);
            }
            else{
                slowo.append(x);
            }
        }
        return slowo.toString();
    }
    public static void main(String[] args) {
        System.out.println("Dana litera występuje "+countChar("hahahehehihi",'h')+" razy");
        System.out.println("Słowo występuje "+ countSubStr("ha ha ha he he hi","ha")+" razy");
        System.out.println("Śroskowe litery to :"+ middle("naleśnik"));
        System.out.println("Śroskowe litery to :"+ middle("szpinak"));
        System.out.println(repeat("la",5));
        System.out.println(where("ha he hi ha ho ha ha","ha"));
        System.out.println(change("LaLaLaLa"));
        System.out.println(nice("12345678"));
        System.out.println(nice1("12345678",".",2));
    }
}

