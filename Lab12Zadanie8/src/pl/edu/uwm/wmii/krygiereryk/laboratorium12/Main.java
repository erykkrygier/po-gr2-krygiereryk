package pl.edu.uwm.wmii.krygiereryk.laboratorium12;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Stack;

public class Main {

    public static void main(String[] args) {
    LinkedList<String> lista = new LinkedList<String>();
    lista.add("Kowalski");
    lista.add("Nowak");
    lista.add("Krygier");
    lista.add("Malinowski");
    lista.add("PanX");
    lista.add("PaniY");
    print(lista);
    Stack<Integer> lista2 = new Stack<>();
    lista2.add(1);
    lista2.add(2);
    lista2.add(3);
    lista2.add(4);
    lista2.add(5);
    lista2.add(6);
    print (lista2);
    }
    public static <E> void print(Iterable<E> obiekt){
        for( E i:obiekt){
            System.out.print(i);
            System.out.print(",");
        }
    }
}
