package pl.edu.uwm.wmii.krygiereryk.laboratorium08;


import pl.imiajd.krygier.Instrument;
import pl.imiajd.krygier.Skrzypce;
import pl.imiajd.krygier.Flet;
import pl.imiajd.krygier.Fortepian;

import java.time.LocalDate;
import java.util.ArrayList;

public class TestInstrumenty {

    public static void main(String[] args) {
        ArrayList<Instrument> orkiestra=new ArrayList<Instrument>();
        orkiestra.add(new Flet("Flet-Pol", LocalDate.parse("2010-04-13")));
        orkiestra.add(new Fortepian("Fortepianex", LocalDate.parse("2015-08-17")));
        orkiestra.add(new Flet("Flet-Pol", LocalDate.parse("2009-12-11")));
        orkiestra.add(new Skrzypce("Skrzypex", LocalDate.parse("2018-04-02")));
        orkiestra.add(new Skrzypce("Skrzypex", LocalDate.parse("2005-05-21")));

        for (Instrument i: orkiestra){
            System.out.println(i.dzwięk());
        }

        System.out.println("Instrumenty:");
        for(Instrument s :orkiestra){
            System.out.println(s.toString());
        }
    }
}
