package pl.imiajd.krygier;

import java.awt.*;

public class BetterRectangle extends Rectangle {
    BetterRectangle (int x,int y,int width, int height){
        super();
        setLocation(x,y);
        setSize(width,height);
    }
    public int getPerimeter (){
        return (2*width+2*height);
    }
    public int GetArea(){
        return (width*height);
    }
}
