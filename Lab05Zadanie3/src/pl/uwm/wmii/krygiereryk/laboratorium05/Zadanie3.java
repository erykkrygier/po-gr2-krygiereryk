package pl.uwm.wmii.krygiereryk.laboratorium05;

import java.util.ArrayList;

public class Zadanie3 {
    public static ArrayList<Integer> mergesort(ArrayList<Integer> a, ArrayList<Integer> b) {

        ArrayList<Integer> nowa = new ArrayList<Integer>();
        ArrayList<Integer> posortowana = new ArrayList<Integer>();
        for (int i = 0; i < a.size(); i++) {
            nowa.add(a.get(i));
        }
        for (int j = 0; j < b.size(); j++) {
            nowa.add(b.get(j));
        }
        int rozmiar=nowa.size();
        System.out.println(nowa);
        ArrayList<Integer> nowakopia = nowa;
        for (int i = 0; i < rozmiar; i++) {
            ArrayList<Integer> najmniejsza = new ArrayList<Integer>();
            najmniejsza.add(nowakopia.get(0));
            for (int j = 0; j < nowakopia.size(); j++) {
                if (nowakopia.get(j) <= najmniejsza.get(0)) {
                    najmniejsza.set(0, nowakopia.get(j));
                }

            }
            posortowana.add(najmniejsza.get(0));
            for(int k=0;k<nowakopia.size();k++){
                if(nowakopia.get(k)==najmniejsza.get(0)){
                    nowakopia.remove(k);
                    break;
                }
            }

        }
        return posortowana;
    }
        public static void main(String[] args) {
            ArrayList<Integer> x = new ArrayList<Integer>();
            x.add(1);
            x.add(4);
            x.add(9);
            x.add(16);
            x.add(5);
            ArrayList<Integer> y = new ArrayList<Integer>();
            y.add(9);
            y.add(7);
            y.add(4);
            y.add(9);
            y.add(11);

            System.out.println(mergesort(x, y));
        }


}