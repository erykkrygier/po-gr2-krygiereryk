package pl.uwm.wmii.krygiereryk.laboratorium04;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class Zadanie3 {
    static int policz(String sciezka,String slowo)throws IOException {
        int suma=0;
        Path nazwapliku = Path.of(sciezka);
        String zdanie= Files.readString(nazwapliku);
        String [] str1=zdanie.split("[ \n]");
        for (int i =0; i< str1.length;i++){
            if (slowo.equals(str1[i])){
                suma++;
            }
        }
        return suma;
    }
    public static void main(String[] args) throws IOException {
    System.out.println("Ilość wystąpień wyrazu wynosi:"+policz("plik1.txt","hello"));

    }
}
