package pl.edu.uwm.wmii.krygiereryk.laboratorium11;



public class ArrayUtil<T> {
    T [] tab;
    public  ArrayUtil (T [] tab){
        this.tab=tab;
    }
    public static <T extends Comparable<T>> int binSearch(T[] arr, T y){
        int temp = 0;
        for(int i=0; i< arr.length; i++){
            if(arr[i].compareTo(y)==0){
                temp = i;
                break;
            }
            else {
                temp = (-1);
            }
        }
        return temp;
    }


}
