package pl.edu.uwm.wmii.krygiereryk.laboratorium11;

public class ArrayUtil <T>{
    T [] tab;
    public ArrayUtil(T[] tab){
        this.tab=tab;
    }
     static <T extends Comparable<T>> void selectionSort(T[] arr){
        T temp = null;
        for(int i=0;i<arr.length;i++){
            for(int j=i+1;j<arr.length;j++){
                if(arr[j].compareTo(arr[i])<0){
                    temp=arr[i];
                    arr[i]=arr[j];
                    arr[j]=temp;
                }
            }
        }
    }
}
