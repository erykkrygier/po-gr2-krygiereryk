package pl.uwm.wmii.krygiereryk.laboratorium06;

public class Zadanie1 {

    public static void main(String[] args) {
	    RachunekBankowy saver1= new RachunekBankowy(2000);
	    RachunekBankowy saver2= new RachunekBankowy(3000);
        RachunekBankowy.setRocznaStopaProcentowa(0.04);
        saver1.ObliczMiesieczneOdsetki();
        saver2.ObliczMiesieczneOdsetki();
        RachunekBankowy.setRocznaStopaProcentowa(0.05);
        saver1.ObliczMiesieczneOdsetki();
        saver2.ObliczMiesieczneOdsetki();
    }
}
