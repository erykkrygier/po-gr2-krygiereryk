package pl.edu.uwm.wmii.krygiereryk.laboratorium13;


import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

class CzescSkladowa implements Comparable<CzescSkladowa>{
    private String nazwaSkladnika;
    private double cenaSkladnika;
    public CzescSkladowa(String nazwaSkladnika,double cenaSkladnika){
        this.cenaSkladnika=cenaSkladnika;
        this.nazwaSkladnika=nazwaSkladnika;
    }

    @Override
    public String toString() {
        return "Nazwa: "+ nazwaSkladnika + " Cena: "+  cenaSkladnika;
    }
    public double getCenaSkladnika(){
        return cenaSkladnika;
    }
    @Override
    public int compareTo(CzescSkladowa o) {
        return this.nazwaSkladnika.compareTo(o.nazwaSkladnika);
    }
}
class salatka{
    private String nazwa;
    private double suma=15;
    private List<CzescSkladowa> skladniki= new ArrayList<CzescSkladowa>();
    private String sos;
    public void dodajSkladnik(CzescSkladowa skladnik){
        skladniki.add(skladnik);
        this.suma = this.suma + skladnik.getCenaSkladnika();
    }
    public void dodajSos(String nazwasosu){
        this.sos=nazwasosu;
    }
    public void ustawNazwe(String nazwasalatki){
        this.nazwa= nazwasalatki;
    }

    @Override
    public String toString() {
        if(!skladniki.isEmpty()){
            return "Sałatka: " +"\n"+ "Nazwa: "+ nazwa +", Cena:" +suma +"\n"+ "Sos: "+ sos +"\n"+ "Suma: "+ suma ;
        }
        return "";
    }
    public boolean czyNazwa(){
        if(nazwa!=null&& !nazwa.trim().isEmpty()){
            return true;
        }
        return false;
    }
    public boolean czyPoprawnaSalatka(){
        if(skladniki.size()>1&& sos!=null && !sos.trim().isEmpty()){
            return true;
        }
        return false;
    }
}
abstract class Zamowienie{
    protected LocalDateTime czasDostawy;
    public boolean poprawnyCzas(){
        if(czasDostawy.compareTo(LocalDateTime.now())>0){
            return true;
        }
        return false;
    }
    public void ustawCzas(LocalDateTime czasx){
        this.czasDostawy= czasx;
    }
}
class NaMiejscu extends Zamowienie{

}
class NaWynos extends Zamowienie{
    @Override
    public boolean poprawnyCzas() {
        if(czasDostawy.getHour() - LocalDateTime.now().getHour()>3){
            return true;
        }
        return false;
    }
}

public class Main {

    public static void main(String[] args) {
        CzescSkladowa salata = new CzescSkladowa("Salata",2.50);
        CzescSkladowa serfeta = new CzescSkladowa("Ser Feta",5.25);
        CzescSkladowa pomidor = new CzescSkladowa("Pomidor",0.75);

        System.out.println("Podaj nazwe salatki");
        Scanner scan = new Scanner(System.in);
        String x = scan.nextLine();
        salatka sal = new salatka();
        sal.ustawNazwe(x);
        if(sal.czyNazwa()){
            while(true){
                System.out.println("Podaj nazwe skladniku");
                System.out.println("1.Salata,2.Pomidory,3.Ser Feta. Wpisz 0 jesli zakonczyles wybor");
                int y= scan.nextInt();
                if(y==1){
                    sal.dodajSkladnik(salata);
                }
                if(y==2){
                    sal.dodajSkladnik(pomidor);
                }
                if(y==3){
                    sal.dodajSkladnik(serfeta);
                }
                if(y==0){
                    break;
                }
            }
        }
    }
}