package pl.edu.uwm.wmii.krygiereryk.laboratorium12;

import java.util.LinkedList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	Scanner scan = new Scanner(System.in);
	int n= scan.nextInt();
	prime(n);
    }
    public static void prime(int n){
        LinkedList<Integer> pierwsze= new LinkedList<Integer>();
        for (int i=2;i<=n;i++){
            pierwsze.add(i);
        }
        for (int j=2;j<=Math.sqrt(n);j++){
            for (int k=0;k<=pierwsze.size()-1;k++){
                if(pierwsze.get(k)%j==0&&pierwsze.get(k)!=j){
                    pierwsze.remove(k);
                }
            }
        }
        System.out.println(pierwsze);
    }
}
