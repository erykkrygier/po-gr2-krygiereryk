package pl.edu.uwm.wmii.krygiereryk.laboratorium11;



public class ArrayUtil<T> {
    T [] tab;
    public  ArrayUtil (T [] tab){
        this.tab=tab;
    }
    static  <T extends Comparable> boolean isSorted(T [] a){
        if (a == null || a.length == 0) {
            return false;
        }
        for(int i=1;i<a.length;i++){
            if(a[i-1].compareTo(a[i])>0){
                return false;
            }
        }
        return true;
    }


}
