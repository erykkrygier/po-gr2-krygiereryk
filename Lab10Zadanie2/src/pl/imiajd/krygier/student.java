package pl.imiajd.krygier;
import java.time.LocalDate;
public class student extends Osoba implements Cloneable,Comparable<Osoba> {
    private double sredniaOcen;

    public student(String nazwisko, LocalDate dataUrodzenia, double sredniaOcen) {
        super(nazwisko, dataUrodzenia);
        this.sredniaOcen = sredniaOcen;
    }

    public String toString() {
        return getClass().getSimpleName() + "[" + getNazwisko() + " " + getDataUrodzenia() + " "+sredniaOcen + "]";
    }

    public boolean equals(Osoba o) {
        if (o.getNazwisko() == getNazwisko() && o.getDataUrodzenia().equals(getDataUrodzenia()) && o.getDataUrodzenia() == getDataUrodzenia()) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int compareTo(Osoba o) {
        int result = super.compareTo((o));
        if ((o instanceof student) && (result == 0)) {
            return -(int) Math.ceil(this.sredniaOcen - ((student) o).sredniaOcen);
        }
        return result;
    }
}