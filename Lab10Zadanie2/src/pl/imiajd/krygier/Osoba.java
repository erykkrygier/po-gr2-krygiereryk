package pl.imiajd.krygier;

import java.time.LocalDate;

public class Osoba implements Cloneable,Comparable<Osoba>{
    private String nazwisko;
    private LocalDate dataUrodzenia;
    public Osoba(String nazwisko,LocalDate dataUrodzenia){
        this.nazwisko=nazwisko;
        this.dataUrodzenia=dataUrodzenia;
    }
    public String getNazwisko(){
        return nazwisko;
    }
    public LocalDate getDataUrodzenia(){
        return dataUrodzenia;
    }
    @Override
    public String toString(){
        return getClass().getSimpleName()+"["+nazwisko+ " "+ dataUrodzenia+"]";
    }

    public boolean equals(Osoba o){
        if (o.getNazwisko()==nazwisko&& o.getDataUrodzenia().equals(dataUrodzenia)){
            return true;
        }
        else{
            return false;
        }
    }
    @Override
    public int compareTo(Osoba o){
        int result =this.nazwisko.compareTo(o.nazwisko);
        if (result!=0){
            return result;
        }
        if (result==0){
            result= this.dataUrodzenia.compareTo(o.dataUrodzenia);

        }
        return result;
    }
}