package pl.imiajd.krygier;

import java.time.LocalDate;

public class Main {

    public static void main(String[] args) {
        LocalDate urodziny1 = LocalDate.of(1965, 12, 5);
        LocalDate urodziny2 = LocalDate.of(1999, 5, 27);
        LocalDate zatrudnienie = LocalDate.of(2009, 4, 13);
        String[] Imiona1 = new String[]{"Jan", "Paweł"};
        String[] Imiona2 = new String[]{"Anna", "Maria"};
        Osoba[] ludzie = new Osoba[2];
        ludzie[0] = new Pracownik("Kowalski", Imiona1, urodziny1, true, 3000.34, zatrudnienie);
        ludzie[1] = new Student("Nowak", Imiona2, urodziny2, false, "Informatyka", 4.25);
        // ludzie[2] = new Osoba("Dyl Sowizdrzał");

        for (Osoba p : ludzie) {
            System.out.println("Nazwisko: " + p.getNazwisko() + " Imiona: " + p.getImiona() + " Data urodzenia: " + p.getDataUrodzenia() + " Płeć:" + p.getPlec() + " " + p.getOpis());
        }
    }
}